import React from 'react';
import PropTypes from 'prop-types';
import bemcn from 'bem-cn';

import Header from '../../containers/header';
import Row from '../../containers/row';

const block = bemcn('table');

const Table = ({ rows, getEmployees }) => (
  <div className={block('container')()}>
    <div className={block('title')()}>
      <h3 className={block('heading')()}>Employee Data</h3>
      <button className={block('btn')()} onClick={getEmployees}>Load</button>
    </div>
    <div>
      {rows.length > 0 && <Header />}
      {rows.map(row => (
        <Row {...row} key={`${row.firstname}${row.surname}`} />
      ))}
    </div>
  </div>
);

Table.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.shape({
    firstname: PropTypes.string,
    surname: PropTypes.string,
    contactNumber: PropTypes.string,
    email: PropTypes.string,
  })),
  getEmployees: PropTypes.func,
};

Table.defaultProps = {
  rows: [],
  getEmployees: () => {},
};

export default Table;
