import { connect } from 'react-redux';

import Component from '../components/table';
import { getEmployees } from '../actions/table';
import { dataSelector } from '../selectors/table';

export const mapStateToProps = state => ({
  rows: dataSelector(state),
});

export const mapDispatchToProps = {
  getEmployees,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
