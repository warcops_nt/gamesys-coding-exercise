import { createSelector } from 'reselect';

export const tableSelector = state => state.table;

export const dataSelector = createSelector(
  tableSelector,
  table => table.data,
);

export const sortingSelector = createSelector(
  tableSelector,
  table => table.sorting,
);

export const columnSelector = createSelector(
  sortingSelector,
  sorting => sorting.column,
);

export const ascendingSelector = createSelector(
  sortingSelector,
  sorting => sorting.ascending,
);
