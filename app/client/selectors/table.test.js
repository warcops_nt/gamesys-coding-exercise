import {
  tableSelector,
  dataSelector,
  sortingSelector,
  columnSelector,
  ascendingSelector,
} from './table';

test('tableSelector > returns table', () => {
  const expected = 'table';
  const actual = tableSelector({
    table: 'table',
  });

  expect(actual).toEqual(expected);
});

test('dataSelector > returns dataSelector', () => {
  const expected = 'data';
  const actual = dataSelector({
    table: {
      data: 'data',
    },
  });

  expect(actual).toEqual(expected);
});

test('sortingSelector > returns dataSelector', () => {
  const expected = 'sorting';
  const actual = sortingSelector({
    table: {
      sorting: 'sorting',
    },
  });

  expect(actual).toEqual(expected);
});

test('columnSelector > returns columnSelector', () => {
  const expected = 'column';
  const actual = columnSelector({
    table: {
      sorting: {
        column: 'column',
      },
    },
  });

  expect(actual).toEqual(expected);
});

test('ascendingSelector > returns columnSelector', () => {
  const expected = 'ascending';
  const actual = ascendingSelector({
    table: {
      sorting: {
        ascending: 'ascending',
      },
    },
  });

  expect(actual).toEqual(expected);
});
