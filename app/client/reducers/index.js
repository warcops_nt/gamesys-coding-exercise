import { combineReducers } from 'redux';

import form from './form';
import table from './table';

export default combineReducers({
  form,
  table,
});
