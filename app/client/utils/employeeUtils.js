export const sortEmployees = (employees, column, ascending) =>
  employees.sort((a, b) => {
    if (a[column] > b[column]) {
      return ascending ? 1 : -1;
    } else if (a[column] < b[column]) {
      return ascending ? -1 : 1;
    }
    return 0;
  });

export const tranformStoreToAPIEmployee = ({
  firstname,
  surname,
  email,
  contactNumber,
}) => ({
  firstname,
  surname,
  email,
  contact_number: contactNumber,
});

export const tranformAPIToStoreEmployee = employee => ({
  firstname: employee.firstname,
  surname: employee.surname,
  email: employee.email,
  contactNumber: employee.contact_number,
});
