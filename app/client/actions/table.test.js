import configureMockStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import thunk from 'redux-thunk';
import axios from 'axios';

import {
  GET_EMPLOYEE_REQUESTED,
  GET_EMPLOYEE_SUCCESS,
  GET_EMPLOYEE_ERROR,
  UPDATE_ORDER,
  getEmployees,
  sortTable,
} from './table';

const mockStore = configureMockStore([thunk]);
let axiosMock;

beforeEach(() => {
  axiosMock = new MockAdapter(axios);
});

afterEach(() => {
  axiosMock.restore();
});

test('getEmployees > gets all employees', () => {
  axiosMock.onGet('/api/employees').reply(200, [
    {
      firstname: 'test',
    },
  ]);
  const store = mockStore({});

  const expectedActions = [
    { type: GET_EMPLOYEE_REQUESTED },
    {
      type: GET_EMPLOYEE_SUCCESS,
      payload: [
        {
          firstname: 'test',
        },
      ],
    },
  ];

  return store.dispatch(getEmployees()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('getEmployees > fails to get all employees', () => {
  axiosMock.onGet('/api/employees').timeout();
  const store = mockStore({});

  const expectedActions = [
    { type: GET_EMPLOYEE_REQUESTED },
    {
      type: GET_EMPLOYEE_ERROR,
      payload: 'Error while getting list of employees',
    },
  ];

  return store.dispatch(getEmployees()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('sortTable > sorts table', () => {
  const store = mockStore({});
  const expectedActions = [
    {
      type: UPDATE_ORDER,
      payload: {
        column: 'test',
        ascending: true,
      },
    },
  ];

  store.dispatch(sortTable('test', true));
  expect(store.getActions()).toEqual(expectedActions);
});
