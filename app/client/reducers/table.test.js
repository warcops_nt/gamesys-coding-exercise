import reducer, { initialState } from './table';
import {
  GET_EMPLOYEE_REQUESTED,
  GET_EMPLOYEE_SUCCESS,
  GET_EMPLOYEE_ERROR,
  UPDATE_ORDER,
} from '../actions/table';
import {
  UPDATE_EMPLOYEE_REQUESTED,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_ERROR,
} from '../actions/form';

test('table reducer > returns initial set upon not matching any action', () => {
  const expected = initialState;
  const actual = reducer(undefined, {
    type: 'random',
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates employees loading status', () => {
  const expected = {
    ...initialState,
    loading: true,
    error: false,
    errorMessage: '',
  };
  const actual = reducer(undefined, {
    type: GET_EMPLOYEE_REQUESTED,
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates employees updating status', () => {
  const expected = {
    ...initialState,
    loading: true,
    error: false,
    errorMessage: '',
  };
  const actual = reducer(undefined, {
    type: UPDATE_EMPLOYEE_REQUESTED,
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates error status for loading employees', () => {
  const expected = {
    ...initialState,
    loading: false,
    error: true,
    errorMessage: 'test error',
  };
  const actual = reducer(undefined, {
    type: GET_EMPLOYEE_ERROR,
    payload: 'test error',
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates error status for updating employees', () => {
  const expected = {
    ...initialState,
    loading: false,
    error: true,
    errorMessage: 'test error',
  };
  const actual = reducer(undefined, {
    type: UPDATE_EMPLOYEE_ERROR,
    payload: 'test error',
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates success status for getting employees', () => {
  const expected = {
    ...initialState,
    loading: false,
    error: false,
    errorMessage: '',
    data: [
      {
        firstname: 'a',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'c',
      },
    ],
  };
  const actual = reducer(undefined, {
    type: GET_EMPLOYEE_SUCCESS,
    payload: [
      {
        firstname: 'a',
      },
      {
        firstname: 'c',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'b',
      },
    ],
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates success status for update employee', () => {
  const expected = {
    ...initialState,
    loading: false,
    error: false,
    errorMessage: '',
  };
  const actual = reducer(undefined, {
    type: UPDATE_EMPLOYEE_SUCCESS,
  });

  expect(actual).toEqual(expected);
});

test('table reducer > updates order for employees', () => {
  const expected = {
    ...initialState,
    data: [
      {
        firstname: 'c',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'a',
      },
    ],
    sorting: {
      column: 'firstname',
      ascending: false,
    },
  };
  const actual = reducer({
    ...initialState,
    data: [
      {
        firstname: 'a',
      },
      {
        firstname: 'c',
      },
      {
        firstname: 'b',
      },
      {
        firstname: 'b',
      },
    ],
  }, {
    type: UPDATE_ORDER,
    payload: {
      column: 'firstname',
      ascending: false,
    },
  });

  expect(actual).toEqual(expected);
});
