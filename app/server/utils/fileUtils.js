const path = require('path');
const util = require('util');
const fs = require('fs');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const fileLoc = path.resolve(
  __dirname,
  '../../../',
  'data',
  'employees.json',
);

module.exports = {
  fileLoc,
  async updateFile(updateFileJSON) {
    const fileData = await readFile(fileLoc);
    const fileJSON = JSON.parse(fileData);
    const updatedFileJSON = updateFileJSON(fileJSON);
    const fileJSONToWrite = updatedFileJSON || fileJSON;
    await writeFile(fileLoc, JSON.stringify(fileJSONToWrite));
  },
};
