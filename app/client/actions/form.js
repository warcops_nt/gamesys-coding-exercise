import axios from 'axios';

import { getEmployees } from './table';
import { tranformStoreToAPIEmployee } from '../utils/employeeUtils';

export const ADD_EMPLOYEE_REQUESTED = 'form/ADD_EMPLOYEE_REQUESTED';
export const ADD_EMPLOYEE_SUCCESS = 'form/ADD_EMPLOYEE_SUCCESS';
export const ADD_EMPLOYEE_ERROR = 'form/ADD_EMPLOYEE_ERROR';
export const UPDATE_EMPLOYEE_REQUESTED = 'form/UPDATE_EMPLOYEE_REQUESTED';
export const UPDATE_EMPLOYEE_SUCCESS = 'form/UPDATE_EMPLOYEE_SUCCESS';
export const UPDATE_EMPLOYEE_ERROR = 'form/UPDATE_EMPLOYEE_ERROR';
export const DELETE_EMPLOYEE_REQUESTED = 'form/DELETE_EMPLOYEE_REQUESTED';
export const DELETE_EMPLOYEE_SUCCESS = 'form/DELETE_EMPLOYEE_SUCCESS';
export const DELETE_EMPLOYEE_ERROR = 'form/DELETE_EMPLOYEE_ERROR';

export const addEmployee = (
  employee,
  resetAll,
) => async (dispatch) => {
  dispatch({ type: ADD_EMPLOYEE_REQUESTED });
  const response = await axios.post('/api/employees', tranformStoreToAPIEmployee(employee));

  const { data: { status, message = '' } } = response;

  if (status === 'SUCCESS') {
    dispatch({ type: ADD_EMPLOYEE_SUCCESS });
    resetAll();
    // eslint-disable-next-line
    alert('Successfully Added Employee Details.');
    dispatch(getEmployees());
  } else {
    // eslint-disable-next-line
    alert(`ERROR: Unable to Add. ${message}`);
    dispatch({ type: ADD_EMPLOYEE_ERROR, payload: message });
  }
};

export const updateEmployee = (
  newValues,
  oldValues,
  resetAll,
) => async (dispatch) => {
  dispatch({ type: UPDATE_EMPLOYEE_REQUESTED });
  const response = await axios.put('/api/employees', {
    oldValues: tranformStoreToAPIEmployee(oldValues),
    updatedValues: tranformStoreToAPIEmployee(newValues),
  });

  const { data: { status, message = '' } } = response;

  if (status === 'SUCCESS') {
    dispatch({ type: UPDATE_EMPLOYEE_SUCCESS });
    resetAll();
    // eslint-disable-next-line
    alert('Successfully Updated Employee Details.');
    dispatch(getEmployees());
  } else {
    // eslint-disable-next-line
    alert(`ERROR: Unable to Update. ${message}`);
    dispatch({ type: UPDATE_EMPLOYEE_ERROR, payload: message });
  }
};


export const deleteEmployee = employee => async (dispatch) => {
  dispatch({ type: DELETE_EMPLOYEE_REQUESTED });
  const response = await axios.delete('/api/employees', {
    data: tranformStoreToAPIEmployee(employee),
  });

  const { data: { status, message = '' } } = response;

  if (status === 'SUCCESS') {
    dispatch({ type: DELETE_EMPLOYEE_SUCCESS });
    // eslint-disable-next-line
    alert('Successfully Deleted Employee Details.');
    dispatch(getEmployees());
  } else {
    // eslint-disable-next-line
    alert(`ERROR: Unable to Delete. ${message}`);
    dispatch({ type: DELETE_EMPLOYEE_ERROR, payload: message });
  }
};
