const logger = require('winston');

const fileUtils = require('../../utils/fileUtils');

module.exports = async (req, res) => {
  try {
    const {
      body: {
        firstname,
        surname,
        email,
      },
    } = req;

    fileUtils.updateFile(employees => employees.filter(employee => (
      employee.firstname !== firstname
      || employee.surname !== surname
      || employee.contact_number !== req.body.contact_number
      || employee.email !== email
    )));
    res.send({ status: 'SUCCESS' });
  } catch (e) {
    logger.error(e);
    res.send({ status: 'ERROR' });
  }
};
