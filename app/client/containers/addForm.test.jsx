import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Container, { mapDispatchToProps } from './addForm';
import { addEmployee } from '../actions/form';

Enzyme.configure({ adapter: new Adapter() });

test('addForm container > maps dispatch to props', () => {
  expect(mapDispatchToProps.onSubmit).toEqual(addEmployee);
});

test('addForm container > loads without error', () => {
  const store = configureStore()({});
  const container = shallow(<Container store={store} />);

  expect(container.length).toEqual(1);
});
