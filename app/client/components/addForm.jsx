import React from 'react';
import PropTypes from 'prop-types';
import { Form, Text } from 'react-form';
import bemcn from 'bem-cn';
import isEmpty from 'lodash/isEmpty';

import {
  validateName,
  validatePhoneNumber,
  validateEmail,
} from '../utils/validators';

const block = bemcn('add-form');

const AddForm = ({ onSubmit }) => (
  <div className={block('container')()}>
    <Form
      onSubmit={(
        submittedValues,
        e,
        formApi,
      ) => onSubmit(submittedValues, formApi.resetAll)}
    >
      { formApi => (
        <form onSubmit={formApi.submitForm}>
          <div className={block('row')()}>
            <label
              htmlFor="add-firstname"
              className={block(
                'field',
                {
                  error: !!(formApi.errors && formApi.errors.firstname),
                },
              )()}
            >
              <span className={block('label')()}>First Name</span>
              <Text
                field="firstname"
                validate={validateName}
                placeholder="First Name"
                id="add-firstname"
                className={block('input')()}
              />
              {formApi.errors
                && formApi.errors.firstname
                && <span className={block('error')()}>{formApi.errors.firstname}</span>}
            </label>
            <label
              htmlFor="add-surname"
              className={block(
                'field',
                {
                  error: !!(formApi.errors && formApi.errors.surname),
                },
              )()}
            >
              <span className={block('label')()}>Surname</span>
              <Text
                field="surname"
                validate={validateName}
                placeholder="Surname"
                id="id-surname"
                className={block('input')()}
              />
              {formApi.errors
                && formApi.errors.surname
                && <span className={block('error')()}>{formApi.errors.surname}</span>}
            </label>
          </div>
          <div className={block('row')()}>
            <label
              htmlFor="add-contact-number"
              className={block(
                'field',
                {
                  error: !!(formApi.errors && formApi.errors.contactNumber),
                },
              )()}
            >
              <span className={block('label')()}>Contact Number</span>
              <Text
                field="contactNumber"
                validate={validatePhoneNumber}
                placeholder="Contact Number"
                id="add-contact-number"
                className={block('input')()}
              />
              {formApi.errors
                && formApi.errors.contactNumber
                && <span className={block('error')()}>{formApi.errors.contactNumber}</span>}
            </label>
            <label
              htmlFor="add-email"
              className={block(
                'field',
                {
                  error: !!(formApi.errors && formApi.errors.email),
                },
              )()}
            >
              <span className={block('label')()}>Email</span>
              <Text
                field="email"
                validate={validateEmail}
                placeholder="Email"
                id="add-email"
                className={block('input')()}
              />
              {formApi.errors
                && formApi.errors.email
                && <span className={block('error')()}>{formApi.errors.email}</span>}
            </label>
          </div>
          <div className={block('row')()}>
            <button
              type="submit"
              className={block('btn', { disabled: isEmpty(formApi.touched) })()}
              disabled={isEmpty(formApi.touched)}
            >
              Add
            </button>
          </div>
        </form>
      )}
    </Form>
  </div>
);

AddForm.propTypes = {
  onSubmit: PropTypes.func,
};

AddForm.defaultProps = {
  onSubmit: () => {},
};

export default AddForm;
