export const required = value => (value ? undefined : 'Required');
export const maxLength = max => value =>
  (value && value.length > max ? `Must be ${max} characters or less` : undefined);
export const maxLength50 = maxLength(50);
export const noSpace = value =>
  (value && !/^\S+$/i.test(value)
    ? 'Name cannot have space'
    : undefined);
export const phoneNumber = value =>
  (value && !/^(\+|0)*([1-9][0-9]{6,14}$)$/i.test(value)
    ? [
      'Invalid phone number, must be between 7 to 15 digits.',
      'You can add "+" or "0" before the number',
    ].join(' ')
    : undefined);
export const email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined);

export const validateName = (value) => {
  const hasValue = required(value);
  const hasLength = maxLength50(value);
  const hasNoSpace = noSpace(value);

  if (hasValue) {
    return {
      error: hasValue,
      success: false,
    };
  }

  if (hasLength) {
    return {
      error: hasLength,
      success: false,
    };
  }

  if (hasNoSpace) {
    return {
      error: hasNoSpace,
      success: false,
    };
  }

  return {
    error: null,
    success: true,
  };
};

export const validatePhoneNumber = (value) => {
  const hasValue = required(value);
  const hasPhoneNumber = phoneNumber(value);

  if (hasValue) {
    return {
      error: hasValue,
      success: false,
    };
  }

  if (hasPhoneNumber) {
    return {
      error: hasPhoneNumber,
      success: false,
    };
  }

  return {
    error: null,
    success: true,
  };
};

export const validateEmail = (value) => {
  const hasValue = required(value);
  const hasEmail = email(value);

  if (hasValue) {
    return {
      error: hasValue,
      success: false,
    };
  }

  if (hasEmail) {
    return {
      error: hasEmail,
      success: false,
    };
  }

  return {
    error: null,
    success: true,
  };
};
