import { connect } from 'react-redux';

import Component from '../components/addForm';
import { addEmployee } from '../actions/form';

export const mapDispatchToProps = {
  onSubmit: addEmployee,
};

export default connect(null, mapDispatchToProps)(Component);
