import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bemcn from 'bem-cn';
import AscendingIcon from 'react-icons/lib/fa/sort-alpha-asc';
import DescendingIcon from 'react-icons/lib/fa/sort-alpha-desc';

const block = bemcn('table-header');

export default class Header extends Component {
  static propTypes = {
    sortTable: PropTypes.func,
    sortingColumn: PropTypes.string,
    ascending: PropTypes.bool,
  };

  static defaultProps = {
    sortTable: () => {},
    sortingColumn: 'firstname',
    ascending: true,
  }

  sort = (column) => {
    const { sortTable, sortingColumn, ascending } = this.props;

    if (column === sortingColumn) {
      sortTable(column, !ascending);
    } else {
      sortTable(column, true);
    }
  }

  render() {
    const { ascending, sortingColumn } = this.props;
    return (
      <div className={block('container')()}>
        <button
          className={block('header').mix('sorting-btn')()}
          onClick={() => this.sort('firstname')}
        >
          First Name
          {sortingColumn === 'firstname' && ascending &&
          <AscendingIcon />}
          {sortingColumn === 'firstname' && !ascending &&
          <DescendingIcon />}
        </button>
        <button
          className={block('header').mix('sorting-btn')()}
          onClick={() => this.sort('surname')}
        >
          Surname
          {sortingColumn === 'surname' && ascending &&
          <AscendingIcon />}
          {sortingColumn === 'surname' && !ascending &&
          <DescendingIcon />}
        </button>
        <button
          className={block('header').mix('sorting-btn')()}
          onClick={() => this.sort('contactNumber')}
        >
          Contact Number
          {sortingColumn === 'contactNumber' && ascending &&
          <AscendingIcon />}
          {sortingColumn === 'contactNumber' && !ascending &&
          <DescendingIcon />}
        </button>
        <button
          className={block('header').mix('sorting-btn')()}
          onClick={() => this.sort('email')}
        >
          Email
          {sortingColumn === 'email' && ascending &&
          <AscendingIcon />}
          {sortingColumn === 'email' && !ascending &&
          <DescendingIcon />}
        </button>
        <div className={block('header')()}>Options</div>
      </div>
    );
  }
}
