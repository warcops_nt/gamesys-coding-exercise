# Gamesys Coding Exercise

> A React-Redux-Express app to maintain employee details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Installing

A step by step series of examples that tell you have to get a development env running

Clone the repository

```
git clone <repo-clone-url>
```
Install the dependencies

```
npm install
```

Build the project

```
npm run build
```

Start the server

```
npm start
```

You should be able to access the application in any browser by accessing `localhost:3000`. You should receive a screen as shown below:

![alt text](./resources/initial.png "Initial Screen")


## Running the tests

Run the following command to execute the coding style tests and unit tests.

```
npm test
```

