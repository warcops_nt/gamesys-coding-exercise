import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';
import App from './components/app';

import './index.scss';

/* eslint-disable */
if (!global._babelPolyfill) {
  require('babel-polyfill');
}

/* eslint-enable */

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.getElementById('app'),
);

if (module.hot) {
  module.hot.accept();
}
