import React from 'react';

import Form from '../containers/addForm';
import Table from '../containers/table';

export default () => (
  <div>
    <Form />
    <Table />
  </div>
);
