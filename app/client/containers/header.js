import { connect } from 'react-redux';

import Component from '../components/table/header';
import { sortTable } from '../actions/table';
import { columnSelector, ascendingSelector } from '../selectors/table';

export const mapStateToProps = state => ({
  sortingColumn: columnSelector(state),
  ascending: ascendingSelector(state),
});

export const mapDispatchToProps = {
  sortTable,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
