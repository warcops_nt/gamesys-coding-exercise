module.exports = class DuplicateException {
  constructor(message) {
    this.message = message;
  }

  getMessage() {
    return this.message;
  }
};
