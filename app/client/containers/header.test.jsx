import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Container, { mapDispatchToProps } from './header';
import { sortTable } from '../actions/table';

Enzyme.configure({ adapter: new Adapter() });

test('addForm container > maps dispatch to props', () => {
  expect(mapDispatchToProps.sortTable).toEqual(sortTable);
});

test('addForm container > loads without error', () => {
  const store = configureStore()({
    table: {
      sorting: {
        column: 'test',
        ascending: true,
      },
    },
  });
  const container = shallow(<Container store={store} />);

  expect(container.length).toEqual(1);
});
