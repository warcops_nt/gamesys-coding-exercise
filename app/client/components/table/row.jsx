import React from 'react';
import PropTypes from 'prop-types';
import { Form, Text } from 'react-form';
import bemcn from 'bem-cn';
import isEmpty from 'lodash/isEmpty';

import {
  validateName,
  validatePhoneNumber,
  validateEmail,
} from '../../utils/validators';

const block = bemcn('table-row');

const EditableRow = ({
  firstname,
  surname,
  email,
  contactNumber,
  updateEmployee,
  deleteEmployee,
  rowId,
}) => (
  <Form
    pure={false}
    defaultValues={{
      firstname,
      surname,
      contactNumber,
      email,
    }}
    onSubmit={(
      submittedValues,
      e,
      formApi,
    ) => updateEmployee(
      submittedValues,
      {
        firstname,
        surname,
        email,
        contactNumber,
      },
      formApi.resetAll,
    )}
  >
    { formApi => (
      <form onSubmit={formApi.submitForm} className={block('container')()}>
        <div className={block('row')()}>
          <label
            htmlFor={`${rowId}-update-firstname`}
            className={block(
              'field',
              {
                error: !!(formApi.errors && formApi.errors.firstname),
              },
            )()}
          >
            <span className={block('label')()}>First Name</span>
            <Text
              field="firstname"
              validate={validateName}
              placeholder="First Name"
              id={`${rowId}-update-firstname`}
              className={block('input')()}
            />
            {formApi.errors
              && formApi.errors.firstname
              && <span className={block('error')()}>{formApi.errors.firstname}</span>}
          </label>
          <label
            htmlFor={`${rowId}-update-surname`}
            className={block(
              'field',
              {
                error: !!(formApi.errors && formApi.errors.surname),
              },
            )()}
          >
            <span className={block('label')()}>Surname</span>
            <Text
              field="surname"
              validate={validateName}
              placeholder="Surname"
              id={`${rowId}-update-surname`}
              className={block('input')()}
            />
            {formApi.errors
              && formApi.errors.surname
              && <span className={block('error')()}>{formApi.errors.surname}</span>}
          </label>
        </div>
        <div className={block('row')()}>
          <label
            htmlFor={`${rowId}-update-contact-number`}
            className={block(
              'field',
              {
                error: !!(formApi.errors && formApi.errors.contactNumber),
              },
            )()}
          >
            <span className={block('label')()}>Contact Number</span>
            <Text
              field="contactNumber"
              validate={validatePhoneNumber}
              placeholder="Contact Number"
              id={`${rowId}-update-contact-number`}
              className={block('input')()}
            />
            {formApi.errors
              && formApi.errors.contactNumber
              && <span className={block('error')()}>{formApi.errors.contactNumber}</span>}
          </label>
          <label
            htmlFor={`${rowId}-update-email`}
            className={block(
              'field',
              {
                error: !!(formApi.errors && formApi.errors.email),
              },
            )()}
          >
            <span className={block('label')()}>Email</span>
            <Text
              field="email"
              validate={validateEmail}
              placeholder="Email"
              id={`${rowId}-update-email`}
              className={block('input')()}
            />
            {formApi.errors
              && formApi.errors.email
              && <span className={block('error')()}>{formApi.errors.email}</span>}
          </label>
        </div>
        <div className={block('row')()}>
          <button
            type="submit"
            className={block('btn', { disabled: isEmpty(formApi.touched) })()}
            disabled={isEmpty(formApi.touched)}
          >
            Update
          </button>
          <button
            className={block('btn', { disabled: isEmpty(formApi.touched) })()}
            onClick={(e) => {
              e.preventDefault();
              formApi.resetAll();
            }}
            disabled={isEmpty(formApi.touched)}
          >
            Cancel
          </button>
          <button
            className={block('btn')()}
            onClick={(e) => {
              e.preventDefault();
              deleteEmployee({
                firstname,
                surname,
                email,
                contactNumber,
              });
            }}
          >
            x
          </button>
        </div>
      </form>
    )}
  </Form>
);

EditableRow.propTypes = {
  updateEmployee: PropTypes.func,
  deleteEmployee: PropTypes.func,
  firstname: PropTypes.string,
  surname: PropTypes.string,
  contactNumber: PropTypes.string,
  email: PropTypes.string,
  rowId: PropTypes.string,
};

EditableRow.defaultProps = {
  updateEmployee: () => {},
  deleteEmployee: () => {},
  firstname: '',
  surname: '',
  contactNumber: '',
  email: '',
  rowId: '',
};

export default EditableRow;
