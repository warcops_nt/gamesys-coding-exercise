import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Component from './app';

Enzyme.configure({ adapter: new Adapter() });

let component;

beforeEach(() => {
  const store = configureStore()({});
  component = shallow(<Component store={store} />);
});

test('app component > loads without error', () => {
  expect(component.length).toEqual(1);
});
