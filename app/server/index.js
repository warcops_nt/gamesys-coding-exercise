const express = require('express');
const path = require('path');
const logger = require('winston');
const bodyParser = require('body-parser');

const apiRouter = require('./api');

const app = express();
app.use(bodyParser.json());
app.unsubscribe(bodyParser.raw());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('dist'));

app.use('/api', apiRouter);

app.get(
  '*',
  (req, res) =>
    res.sendFile(path.resolve(
      __dirname,
      '../../',
      'dist',
      'index.html',
    )),
);
app.listen(3000, () => logger.info('Listening to port 3000'));
