const express = require('express');
const employees = require('./employees');

const router = express.Router();

router.use('/employees', employees);
module.exports = router;
