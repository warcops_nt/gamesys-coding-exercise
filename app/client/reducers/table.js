import {
  GET_EMPLOYEE_REQUESTED,
  GET_EMPLOYEE_SUCCESS,
  GET_EMPLOYEE_ERROR,
  UPDATE_ORDER,
} from '../actions/table';
import {
  UPDATE_EMPLOYEE_REQUESTED,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_ERROR,
} from '../actions/form';

import { sortEmployees, tranformAPIToStoreEmployee } from '../utils/employeeUtils';

export const initialState = {
  loading: false,
  error: false,
  errorMessage: '',
  data: [],
  sorting: {
    column: 'firstname',
    ascending: true,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_EMPLOYEE_REQUESTED:
    case UPDATE_EMPLOYEE_REQUESTED:
      return {
        ...state,
        error: false,
        loading: true,
        errorMessage: '',
      };
    case GET_EMPLOYEE_ERROR:
    case UPDATE_EMPLOYEE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload,
      };
    case GET_EMPLOYEE_SUCCESS: {
      const { column, ascending } = state.sorting;
      const unsortedData = action.payload.map(employee => tranformAPIToStoreEmployee(employee));
      const data = sortEmployees(unsortedData, column, ascending);

      return {
        ...state,
        loading: false,
        error: false,
        errorMessage: '',
        data,
      };
    }
    case UPDATE_EMPLOYEE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        errorMessage: '',
      };
    case UPDATE_ORDER: {
      const { column, ascending } = action.payload;
      const unsortedData = state.data.map(d => ({ ...d }));
      const data = sortEmployees(unsortedData, column, ascending);

      return {
        ...state,
        data,
        sorting: {
          ...state.sorting,
          ascending,
          column,
        },
      };
    }
    default:
      return state;
  }
};
