import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Container, { mapDispatchToProps } from './table';
import { getEmployees } from '../actions/table';

Enzyme.configure({ adapter: new Adapter() });

test('table container > maps dispatch to props', () => {
  expect(mapDispatchToProps.getEmployees).toEqual(getEmployees);
});

test('table container > loads without error', () => {
  const store = configureStore()({
    table: {
      data: [{
        firstname: 'test',
        surname: 'test',
        email: 'test@test.com',
        contactNumber: '1234',
      }],
    },
  });
  const container = shallow(<Container store={store} />);

  expect(container.length).toEqual(1);
});
