import configureMockStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import thunk from 'redux-thunk';
import axios from 'axios';

import {
  addEmployee,
  updateEmployee,
  deleteEmployee,
  ADD_EMPLOYEE_REQUESTED,
  ADD_EMPLOYEE_SUCCESS,
  ADD_EMPLOYEE_ERROR,
  UPDATE_EMPLOYEE_REQUESTED,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_ERROR,
  DELETE_EMPLOYEE_REQUESTED,
  DELETE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_ERROR,
} from './form';
import { GET_EMPLOYEE_REQUESTED } from './table';

const mockStore = configureMockStore([thunk]);
let axiosMock;

beforeEach(() => {
  axiosMock = new MockAdapter(axios);
});

afterEach(() => {
  axiosMock.restore();
});

test('addEmployee > adds new employee', () => {
  const employee = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const resetAll = jest.fn();
  axiosMock.onPost('/api/employees').reply(200, {
    status: 'SUCCESS',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: ADD_EMPLOYEE_REQUESTED },
    { type: ADD_EMPLOYEE_SUCCESS },
    { type: GET_EMPLOYEE_REQUESTED },
  ];

  return store.dispatch(addEmployee(employee, resetAll)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('addEmployee > fails to add new employee', () => {
  const employee = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const resetAll = jest.fn();
  axiosMock.onPost('/api/employees').reply(200, {
    status: 'ERROR',
    message: 'error message',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: ADD_EMPLOYEE_REQUESTED },
    { type: ADD_EMPLOYEE_ERROR, payload: 'error message' },
  ];

  return store.dispatch(addEmployee(employee, resetAll)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('updateEmployee > updates employee', () => {
  const oldValues = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const newValues = {
    firstname: 'testfirst123',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const resetAll = jest.fn();
  axiosMock.onPut('/api/employees').reply(200, {
    status: 'SUCCESS',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: UPDATE_EMPLOYEE_REQUESTED },
    { type: UPDATE_EMPLOYEE_SUCCESS },
    { type: GET_EMPLOYEE_REQUESTED },
  ];

  return store.dispatch(updateEmployee(oldValues, newValues, resetAll)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('updateEmployee > fails to update employee', () => {
  const oldValues = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const newValues = {
    firstname: 'testfirst123',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };
  const resetAll = jest.fn();
  axiosMock.onPut('/api/employees').reply(200, {
    status: 'ERROR',
    message: 'error message',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: UPDATE_EMPLOYEE_REQUESTED },
    { type: UPDATE_EMPLOYEE_ERROR, payload: 'error message' },
  ];

  return store.dispatch(updateEmployee(oldValues, newValues, resetAll)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('deleteEmployee > deletes employee', () => {
  const employee = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };

  axiosMock.onDelete('/api/employees').reply(200, {
    status: 'SUCCESS',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: DELETE_EMPLOYEE_REQUESTED },
    { type: DELETE_EMPLOYEE_SUCCESS },
    { type: GET_EMPLOYEE_REQUESTED },
  ];

  return store.dispatch(deleteEmployee(employee)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('deleteEmployee > fails to delete employee', () => {
  const employee = {
    firstname: 'testfirst',
    surname: 'testsur',
    email: 'test@email.com',
    contactNumber: '123456789',
  };

  axiosMock.onDelete('/api/employees').reply(200, {
    status: 'ERROR',
    message: 'error message',
  });
  const store = mockStore({});

  const expectedActions = [
    { type: DELETE_EMPLOYEE_REQUESTED },
    { type: DELETE_EMPLOYEE_ERROR, payload: 'error message' },
  ];

  return store.dispatch(deleteEmployee(employee)).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
