const express = require('express');
const getHandler = require('./get');
const postHandler = require('./post');
const deleteHandler = require('./delete');
const putHandler = require('./put');

const router = express.Router();

router.get('/', getHandler);
router.post('/', postHandler);
router.delete('/', deleteHandler);
router.put('/', putHandler);
module.exports = router;
