const logger = require('winston');

const fileUtils = require('../../utils/fileUtils');
const DuplicateException = require('../../utils/DuplicateException');

module.exports = async (req, res) => {
  try {
    const {
      body: {
        firstname,
        surname,
        email,
      },
    } = req;

    await fileUtils.updateFile((employees) => {
      const match = employees.filter(employee => (
        employee.firstname === firstname
          && employee.surname === surname
      ));

      if (match.length > 0) {
        throw new DuplicateException('Cannot add entry. Duplicate name.');
      }

      return employees.concat([{
        firstname,
        surname,
        contact_number: req.body.contact_number,
        email,
      }]);
    });
    res.send({ status: 'SUCCESS' });
  } catch (e) {
    if (e instanceof DuplicateException) {
      const message = e.getMessage();
      res.send({ status: 'ERROR', message });
    } else {
      logger.error(e);
      res.send({ status: 'ERROR', message: 'Error while creating record.' });
    }
  }
};
