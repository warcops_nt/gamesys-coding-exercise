import { connect } from 'react-redux';
import uuid from 'uuid/v4';

import Component from '../components/table/row';
import { updateEmployee, deleteEmployee } from '../actions/form';

export const mapStateToProps = () => ({
  rowId: uuid(),
});

export const mapDispatchToProps = {
  updateEmployee,
  deleteEmployee,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
