import reducer, { initialState } from './form';
import {
  ADD_EMPLOYEE_REQUESTED,
  ADD_EMPLOYEE_SUCCESS,
  ADD_EMPLOYEE_ERROR,
} from '../actions/form';

test('form reducer > returns initial set upon not matching any action', () => {
  const expected = initialState;
  const actual = reducer(undefined, {
    type: 'random',
  });

  expect(actual).toEqual(expected);
});

test('form reducer > updates loading status', () => {
  const expected = {
    loading: true,
    error: false,
    errorMessage: '',
  };
  const actual = reducer(undefined, {
    type: ADD_EMPLOYEE_REQUESTED,
  });

  expect(actual).toEqual(expected);
});

test('form reducer > updates error status', () => {
  const expected = {
    loading: false,
    error: true,
    errorMessage: 'test error',
  };
  const actual = reducer(undefined, {
    type: ADD_EMPLOYEE_ERROR,
    payload: 'test error',
  });

  expect(actual).toEqual(expected);
});

test('form reducer > updates success status', () => {
  const expected = {
    loading: false,
    error: false,
    errorMessage: '',
  };
  const actual = reducer(undefined, {
    type: ADD_EMPLOYEE_SUCCESS,
  });

  expect(actual).toEqual(expected);
});
