import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Container, { mapStateToProps, mapDispatchToProps } from './row';
import { updateEmployee, deleteEmployee } from '../actions/form';

Enzyme.configure({ adapter: new Adapter() });

test('row container > maps state to props', () => {
  const actual = mapStateToProps();
  expect(actual).toHaveProperty('rowId');
});

test('row container > maps dispatch to props', () => {
  expect(mapDispatchToProps.updateEmployee).toEqual(updateEmployee);
  expect(mapDispatchToProps.deleteEmployee).toEqual(deleteEmployee);
});

test('row container > loads without error', () => {
  const store = configureStore()({});
  const container = shallow(<Container store={store} />);

  expect(container.length).toEqual(1);
});
