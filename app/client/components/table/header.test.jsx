import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Component from './header';

Enzyme.configure({ adapter: new Adapter() });

let component;
let onSubmit;

beforeEach(() => {
  const store = configureStore()({});
  onSubmit = jest.fn();
  component = shallow(<Component store={store} onSubmit={onSubmit} />);
});

test('header component > loads without error', () => {
  expect(component.length).toEqual(1);
});

