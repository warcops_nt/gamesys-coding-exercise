import {
  sortEmployees,
  tranformStoreToAPIEmployee,
  tranformAPIToStoreEmployee,
} from './employeeUtils';

test('sortEmployees > sort in ascending order for string column', () => {
  const expected = [
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
    {
      firstname: 'b1',
      surname: 'b2',
      email: 'b3@b3.com',
      contactNumber: 987654321,
    },
  ];

  const actual = sortEmployees(
    [
      {
        firstname: 'b1',
        surname: 'b2',
        email: 'b3@b3.com',
        contactNumber: 987654321,
      },
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
    ],
    'firstname',
    true,
  );

  expect(actual).toEqual(expected);
});

test('sortEmployees > sort in descending order for string column', () => {
  const expected = [
    {
      firstname: 'b1',
      surname: 'b2',
      email: 'b3@b3.com',
      contactNumber: 987654321,
    },
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
  ];

  const actual = sortEmployees(
    [
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
      {
        firstname: 'b1',
        surname: 'b2',
        email: 'b3@b3.com',
        contactNumber: 987654321,
      },
    ],
    'firstname',
    false,
  );

  expect(actual).toEqual(expected);
});

test('sortEmployees > sort in ascending order for numeric column', () => {
  const expected = [
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
    {
      firstname: 'b1',
      surname: 'b2',
      email: 'b3@b3.com',
      contactNumber: 987654321,
    },
  ];

  const actual = sortEmployees(
    [
      {
        firstname: 'b1',
        surname: 'b2',
        email: 'b3@b3.com',
        contactNumber: 987654321,
      },
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
    ],
    'firstname',
    true,
  );

  expect(actual).toEqual(expected);
});

test('sortEmployees > sort in descending order for numeric column', () => {
  const expected = [
    {
      firstname: 'b1',
      surname: 'b2',
      email: 'b3@b3.com',
      contactNumber: 987654321,
    },
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
  ];

  const actual = sortEmployees(
    [
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
      {
        firstname: 'b1',
        surname: 'b2',
        email: 'b3@b3.com',
        contactNumber: 987654321,
      },
    ],
    'firstname',
    false,
  );

  expect(actual).toEqual(expected);
});

test('sortEmployees > maintain order if values are same', () => {
  const expected = [
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
    {
      firstname: 'a1',
      surname: 'a2',
      email: 'a3@a3.com',
      contactNumber: 123456789,
    },
  ];

  const actual = sortEmployees(
    [
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
      {
        firstname: 'a1',
        surname: 'a2',
        email: 'a3@a3.com',
        contactNumber: 123456789,
      },
    ],
    'firstname',
    false,
  );

  expect(actual).toEqual(expected);
});

test('tranformStoreToAPIEmployee > transform to API format', () => {
  const expected = {
    firstname: 'test1',
    surname: 'test2',
    email: 'test@test.com',
    contact_number: 123456789,
  };

  const actual = tranformStoreToAPIEmployee({
    firstname: 'test1',
    surname: 'test2',
    email: 'test@test.com',
    contactNumber: 123456789,
  });

  expect(actual).toEqual(expected);
});

test('tranformAPIToStoreEmployee > transform to store format', () => {
  const expected = {
    firstname: 'test1',
    surname: 'test2',
    email: 'test@test.com',
    contactNumber: 123456789,
  };

  const actual = tranformAPIToStoreEmployee({
    firstname: 'test1',
    surname: 'test2',
    email: 'test@test.com',
    contact_number: 123456789,
  });

  expect(actual).toEqual(expected);
});
