import axios from 'axios';

export const GET_EMPLOYEE_REQUESTED = 'table/GET_EMPLOYEE_REQUESTED';
export const GET_EMPLOYEE_SUCCESS = 'table/GET_EMPLOYEE_SUCCESS';
export const GET_EMPLOYEE_ERROR = 'table/GET_EMPLOYEE_ERROR';
export const UPDATE_ORDER = 'table/UPDATE_ORDER';

export const getEmployees = () => async (dispatch) => {
  dispatch({ type: GET_EMPLOYEE_REQUESTED });
  try {
    const response = await axios.get('/api/employees');

    const employees = response.data;
    dispatch({ type: GET_EMPLOYEE_SUCCESS, payload: employees });
  } catch (e) {
    // eslint-disable-next-line
    alert('ERROR: Unable to get List of Employees');
    dispatch({ type: GET_EMPLOYEE_ERROR, payload: 'Error while getting list of employees' });
  }
};

export const sortTable = (column, ascending) => ({
  type: UPDATE_ORDER,
  payload: {
    column,
    ascending,
  },
});
