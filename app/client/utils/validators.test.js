import {
  required,
  maxLength,
  maxLength50,
  noSpace,
  phoneNumber,
  email,
  validateName,
  validatePhoneNumber,
  validateEmail,
} from './validators';

test('required > succeed if value is present', () => {
  const actual = required('test');

  expect(actual).toBeUndefined();
});

test('required > fail if value not present', () => {
  const expected = 'Required';
  const actual = required('');

  expect(actual).toEqual(expected);
});

test('maxLength > succeed if value in length', () => {
  const actual = maxLength(10)('test');

  expect(actual).toBeUndefined();
});

test('maxLength > fail if value beyond length', () => {
  const expected = 'Must be 10 characters or less';
  const actual = maxLength(10)('test of something larger');

  expect(actual).toEqual(expected);
});

test('maxLength50 > succeed if value in length of 50 chars', () => {
  const actual = maxLength50('test');

  expect(actual).toBeUndefined();
});

test('maxLength > fail if value beyond length of 50 chars', () => {
  const expected = 'Must be 50 characters or less';
  const actual = maxLength50(`test of something larger than 50.
  because only then shall we know if this test is going to pass.`);

  expect(actual).toEqual(expected);
});

test('noSpace > succeed if value has no space', () => {
  const actual = noSpace('test');

  expect(actual).toBeUndefined();
});

test('noSpace > fail if value has space', () => {
  const expected = 'Name cannot have space';
  const actual = noSpace('test space');

  expect(actual).toEqual(expected);
});

test('phoneNumber > succeed if value matches a phone number', () => {
  const actual = phoneNumber('1234567');

  expect(actual).toBeUndefined();
});

test('phoneNumber > fail if value doesnt match a phone number', () => {
  const actual = phoneNumber('123');

  expect(actual).not.toBeUndefined();
});

test('email > succeed if value is email', () => {
  const actual = email('test@test.com');

  expect(actual).toBeUndefined();
});

test('email > fail if value is not email', () => {
  const expected = 'Invalid email address';
  const actual = email('test');

  expect(actual).toEqual(expected);
});

test('validateName > succeed if value is present without space and within length', () => {
  const expected = {
    error: null,
    success: true,
  };
  const actual = validateName('test');

  expect(actual).toEqual(expected);
});

test('validateName > fail if value not present', () => {
  const expected = {
    error: 'Required',
    success: false,
  };
  const actual = validateName('');

  expect(actual).toEqual(expected);
});

test('validateName > fail if value has space', () => {
  const expected = {
    error: 'Name cannot have space',
    success: false,
  };
  const actual = validateName('test space');

  expect(actual).toEqual(expected);
});

test('validateName > fail if value larger than 50 chars', () => {
  const expected = {
    error: 'Must be 50 characters or less',
    success: false,
  };
  const actual = validateName(`test of something larger than 50.
  because only then shall we know if this test is going to pass.`);

  expect(actual).toEqual(expected);
});

test('validatePhoneNumber > succeed if value is phone number', () => {
  const expected = {
    error: null,
    success: true,
  };
  const actual = validatePhoneNumber('123456789');

  expect(actual).toEqual(expected);
});

test('validatePhoneNumber > fail if value not present', () => {
  const expected = {
    error: 'Required',
    success: false,
  };
  const actual = validatePhoneNumber('');

  expect(actual).toEqual(expected);
});

test('validatePhoneNumber > fail if value not phone number', () => {
  const expected = {
    error: null,
    success: true,
  };
  const actual = validatePhoneNumber('123');

  expect(actual).not.toEqual(expected);
});

test('validateEmail > succeed if value is email', () => {
  const expected = {
    error: null,
    success: true,
  };
  const actual = validateEmail('test@test.com');

  expect(actual).toEqual(expected);
});

test('validateEmail > fail if value not present', () => {
  const expected = {
    error: 'Required',
    success: false,
  };
  const actual = validateEmail('');

  expect(actual).toEqual(expected);
});

test('validateEmail > fail if value not email', () => {
  const expected = {
    error: 'Invalid email address',
    success: false,
  };
  const actual = validateEmail('test');

  expect(actual).toEqual(expected);
});
