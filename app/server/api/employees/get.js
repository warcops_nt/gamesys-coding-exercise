const fileUtils = require('../../utils/fileUtils');

module.exports = (req, res) => res.sendFile(fileUtils.fileLoc);
