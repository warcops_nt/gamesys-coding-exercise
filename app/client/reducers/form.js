import {
  ADD_EMPLOYEE_REQUESTED,
  ADD_EMPLOYEE_SUCCESS,
  ADD_EMPLOYEE_ERROR,
} from '../actions/form';

export const initialState = {
  loading: false,
  error: false,
  errorMessage: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_EMPLOYEE_REQUESTED:
      return {
        ...state,
        loading: true,
        error: false,
        errorMessage: '',
      };
    case ADD_EMPLOYEE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload,
      };
    case ADD_EMPLOYEE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        errorMessage: '',
      };
    default:
      return state;
  }
};
