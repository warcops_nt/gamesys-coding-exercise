const logger = require('winston');

const fileUtils = require('../../utils/fileUtils');
const DuplicateException = require('../../utils/DuplicateException');

module.exports = async (req, res) => {
  try {
    const {
      body: {
        oldValues,
        updatedValues,
      },
    } = req;

    await fileUtils.updateFile((employees) => {
      const match = employees.filter(employee => (
        employee.firstname === updatedValues.firstname
          && employee.surname === updatedValues.surname
      ));

      if (match.length > 0) {
        throw new DuplicateException('Cannot update entry. Duplicate name.');
      }
      return employees.map((employee) => {
        if (
          employee.firstname !== oldValues.firstname
          || employee.surname !== oldValues.surname
          || employee.contact_number !== oldValues.contact_number
          || employee.email !== oldValues.email
        ) {
          return employee;
        }

        return {
          firstname: updatedValues.firstname,
          surname: updatedValues.surname,
          contact_number: updatedValues.contact_number,
          email: updatedValues.email,
        };
      });
    });
    res.send({ status: 'SUCCESS' });
  } catch (e) {
    if (e instanceof DuplicateException) {
      const message = e.getMessage();
      res.send({ status: 'ERROR', message });
    } else {
      logger.error(e);
      res.send({ status: 'ERROR', message: 'Error while updating record.' });
    }
  }
};
